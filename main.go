// Subsample SBF records with a specified block number.
package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"

	sbf "bitbucket.org/uwaploe/go-sbf"
)

var usage = `Usage: subsbf [options] BLOCKNUM N

Subsample an SBF file extracting every Nth record of type BLOCKNUM. The
output is written to standard output.
`
var (
	inFile = flag.String("in", "", "Read SBF records from a file")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	args := flag.Args()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		file        io.Reader
		err         error
		blocknum, N int
	)

	blocknum, _ = strconv.Atoi(args[0])
	N, _ = strconv.Atoi(args[1])

	if *inFile != "" {
		file, err = os.Open(*inFile)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		file = os.Stdin
	}

	scanner := sbf.NewScanner(file)
	matches := int(0)
	for i := 1; scanner.Scan(); i++ {
		b := scanner.Block()
		if int(b.BlockNum()) == blocknum {
			if (matches % N) == 0 {
				os.Stdout.Write(b.Raw())
			}
			matches++
		}
	}

	if err = scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
